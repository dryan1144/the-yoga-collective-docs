# Yoga Collective API Documentation

## Contents

1. [Overview](#overview)
2. [Authentication](#authentication)
3. [Users](#users)
4. [Collections](#collections)
5. [Favorites](#favorites)
6. [Classes](#classes)
7. [Filters and Search](#filters)
8. [Teachers](#teachers)
9. [Blog](#blog)
10. [Utility Taxonomies ](#utilities)
11. [Quick Hits ](#quickhits)
12. [Reminders](#reminders)



## Overview

The Yoga Collective API is based on the WordPress REST API with a combination of default and custom endpoints. Requests made by `GET` are unauthenticated, whereas `POST` requests will require authentication using JSON Web Tokens. Unauthenticated `POST` requests will return a `401` error code.



### Data Structure

| Content Type | Object Type      | Description                                       |
| ------------ | ---------------- | ------------------------------------------------- |
| Classes      | Custom Post Type | Content protected by MemberPress                  |
| Collections  | Custom Post Type | Posts with an array of attached Classes           |
| Teachers     | Taxonomy  | Custom taxonomy attached to Classes & Collections |
| Style        | Taxonomy  | Custom taxonomy attached to Classes               |
|Duration|Taxonomy|Custom taxonomy attached to Classes|
|Focus|Taxonomy|Custom taxonomy attached to Classes|
|Class Type|Taxonomy|Custom taxonomy attached to Classes|
|Featured|Taxonomy|Hidden taxonomy used for Classes and Collections|
|Featured:free| Term |Allows users to view Class on website while logged out|
|Featured:homepage| Term |Used for showing Classes on logged-in homepage under "Trending", and Collections under "Featured Collections"|



### Endpoints List

| Endpoint                                | Method |
| --------------------------------------- | ------ |
| /wp-json/jwt-auth/v1/token/login        |        |
| /wp-json/jwt-auth/v1/token/validate     |        |
| /wp-json/jwt-auth/v1/token/refresh |        |
| /wp-json/wp/v2/users/<userid>           |        |
| /wp-json/wp/v2/classes/                 |        |
| /wp-json/wp/v2/classes/<classid>        |        |
| /wp-json/yc-favorites/v1/user/<userid> |        |
|/wp-json/yc-favorites/v1/user/<userid>/?class=<classid>||
|/wp-json/wp/v2/collections/||
|/wp-json/wp/v2/collections/<collectionid>||
|/wp-json/yc-collections/v1/start/<userid>?collection=<collectionid>||
|/wp-json/yc-collections/v1/remove/<userid>?collection=<collectionid>||
|/wp-json/yc-collections/v1/user/<userid>||
|/wp-json/yc-collections/v1/user/<userid>/?class=<classid>&collection=<collectionid>||
|/wp-json/wp/v2/teachers/||
|/wp-json/wp/v2/teachers/<teacherid>||
|/wp-json/wp/v2/posts/<id>||
|/wp-json/wp/v2/media/<mediaid>||
|/wp-json/yc-users/v1/register/||
|/wp-json/yc-users/v1/register/update/||
|/wp-json/yc-users/v1/register/validate/||
|/wp-json/yc-users/v1/lostpassword/||
|/wp-json/yc-users/v1/delete/||
|/wp-json/yc-reminders/v1/user/<userid>||
|/wp-json/yc-filters/v1/refresh-taxonomies||
| /wp-json/yc-filters/v1/move-taxonomies                       ||
|/wp-json/yc-filters/v1/connect-taxonomies||
|/wp-json/yc-focus/v1/featured||



## Authentication

Web tokens are handled through the [JWT Authentication for WP REST API](https://github.com/Tmeister/wp-api-jwt-auth) plugin. Send username and password in body to receive a token and then use Bearer token in Authorization header.

### Login User

`/wp-json/jwt-auth/v1/token/login`

Send user credentials body and endpoint will return a token to use in a future request to the API if the authentication is correct or error if the authentication fails.

**Body Parameters**

| Parameter | Type     | Description                                |
| --------- | -------- | ------------------------------------------ |
| username  | <string> | Username or email                          |
| password  | <string> | User password                              |
| OS        | <string> | device being logged in on - android or ios |

**Example Response**

```json
//successful login
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95b2dhY29sbGVjdGl2ZS5kZXYuY2MiLCJpYXQiOjE1NTIwMzA5MDEsIm5iZiI6MTU1MjAzMDkwMSwiZXhwIjoxNTU3MjE0OTAxLCJkYXRhIjp7InVzZXIiOnsiaWQiOiIxIn19fQ.GwIqCa3yyadMctjx_2nFqx6Lz97aK95w7UC4bk2dMHw",
    "user_email": "dave@healingartswebdesign.com",
    "user_nicename": "webdev-2",
    "user_display_name": "Web Dev",
    "first_name": "TestAPI",
    "last_name": "Ryan",
    "user_id": "1",
    "token_expiration": 1557214901, // 1 day
  	"refresh_token" : "yJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95b2dhY29sbGVjdGl2ZS5kZXYuY2MiLCJpYXQiOjE1NTIwMzA5MDEsIm5iZiI6MTU1Maew7f853a7efhhaefbaBBfaefjx_2nFqxaefhuhuiahefiuha7e",
  	"refresh_token_expiration" : 15589334345 // 60 days
}

//login error
{
    "code": "invalid_login",
    "message": "Invalid username or password",
    "data": {
        "status": 403
    }
}

//expired purchase token
{
    "code": "token_expired",
    "message": "This email has an existing Android subscription. Please login from your Android device or use another email.",
    "data": {
        "status": 403,
        "OS": "android" //platform user used to register - android, ios, or website
    }
}

//invalid purchase token
{
    "code": "token_error",
    "message": "Invalid purchase token",
    "data": {
        "status": 403,
        "OS": "android" //platform user used to register - android, ios, or website
    }
}
```



### Validate Token (JWT)

Make a POST request sending the Authorization header to validate a token.

`/wp-json/jwt-auth/v1/token/validate`

**Example Response**

```json
//valid token
{
    "code": "jwt_auth_valid_token",
    "data": {
        "status": 200
    }
}

//invalid token (JWT)
{
    "code": "jwt_auth_invalid_token",
    "message": "Wrong number of segments",
    "data": {
        "status": 403
    }
}
```



### Refresh Token (JWT)

`/wp-json/jwt-auth/v1/token/refresh/`

You only will need to make a POST request sending the token in the Authorization header. Response is same as login endpoint.



## Users

### Get user data

| Method | Endpoint                      |
| ------ | ----------------------------- |
| GET    | /wp-json/wp/v2/users/<userid> |

**Selected JSON Response**

| Attribute                 | Description                                                  |
| ------------------------- | ------------------------------------------------------------ |
| favorites:posts           | <array> IDs of favorited Classes. See Favorites for alternative endpoint |
| yc_user_completed_classes | <array> User's Collections. See Collections for alternative endpoint |

**Example Response**

`/wp-json/wp/v2/users/1`

```json
   "id":1,
   "name":"Web Dev",
   "url":"http:\/\/yogacollective.com",
   "description":"This is a test",
   "link":"http:\/\/yogacollective.dev.cc\/author\/webdev\/",
   "slug":"webdev",
   "avatar_urls":{
      "24":"http:\/\/2.gravatar.com\/avatar\/25b3ce3b4409e0b303bee9a38aaea423?s=24&d=mm&r=g",
      "48":"http:\/\/2.gravatar.com\/avatar\/25b3ce3b4409e0b303bee9a38aaea423?s=48&d=mm&r=g",
      "96":"http:\/\/2.gravatar.com\/avatar\/25b3ce3b4409e0b303bee9a38aaea423?s=96&d=mm&r=g"
   },
   "meta":[],
   "favorites":[
      {
         "site_id":1,
         "posts":[
            9241,
            9285
         ],
         "groups":[]
      }
   ],
   "yc_user_completed_classes":{
	  "data": {
	      "323":{
	         "classes":[
	            9298,
	            9296
	         ],
	         "status":"complete",
	      },
	      "9309":{
	         "classes":null,
	         "status":null,
	      },
	  }
   },
}
```

### Edit existing user

| Method | Endpoint                      |
| ------ | ----------------------------- |
| POST   | /wp-json/wp/v2/users/<userid> |

**Parameters**

| Parameter  | Type     | Description                         |
| ---------- | -------- | ----------------------------------- |
| first_name | <string> | First Name                          |
| last_name  | <string> | Last Name                           |
| email      | <string> | Email address (must be valid email) |

**JSON Response**

Updated user ouput or WP error



### Validate user email

Check to see if a user email already exists before registering a user

| Method | Endpoint                               |
| ------ | -------------------------------------- |
| POST   | /wp-json/yc-users/v1/register/validate |

**Required Parameters**

| Parameter | Type     |
| --------- | -------- |
| email     | <string> |

**Example Response**

`/wp-json/yc-users/v1/register/validate/?email=testuser@gmail.com`

```json
//user exists
{
    "code": "user_exists",
    "message": "User exists",
    "user_data": {
        "user_id": 1,
        "OS": "website"
    }
}

//user does not exist
{
    "code": "new_user",
    "message": "Email does not exist",
}

//errors
{
    "code": "invalid_email",
    "message": "Invalid email address",
    "data": {
        "status": 404
    }
}
```



### Register new user

| Method | Endpoint                       |
| ------ | ------------------------------ |
| POST   | /wp-json/yc-users/v1/register/ |

**Required Parameters (Sent in Body)** 

| Parameter     | Type     | Description                                             |
| ------------- | -------- | ------------------------------------------------------- |
| email         | <string> | user ID for which data is being requested               |
| first         | <string> | user first name                   |
| last          | <string> | user last name                   |
| pw            | <string> | plaintext password                                      |
| productId     | <string> | yoga_monthly, yoga_quarterly, yoga_yearly               |
| OS            | <string> | must be 'ios' or 'android'                              |
| purchaseToken | <string> | token provided by app store for subscription validation |

**Example Response**

`/wp-json/yc-users/v1/register/`

```json
//success
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95b2dhY29sbGVjdGl2ZS5kZXYuY2MiLCJpYXQiOjE1NTIwMzA5MDEsIm5iZiI6MTU1MjAzMDkwMSwiZXhwIjoxNTU3MjE0OTAxLCJkYXRhIjp7InVzZXIiOnsiaWQiOiIxIn19fQ.GwIqCa3yyadMctjx_2nFqx6Lz97aK95w7UC4bk2dMHw",
    "user_email": "dave@healingartswebdesign.com",
    "user_nicename": "webdev-2",
    "user_display_name": "Web Dev",
    "first_name": "TestAPI",
    "last_name": "Ryan",
    "user_id": "1",
    "token_expiration": 1557214901, // 1 day
  	"refresh_token" : "yJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95b2dhY29sbGVjdGl2ZS5kZXYuY2MiLCJpYXQiOjE1NTIwMzA5MDEsIm5iZiI6MTU1Maew7f853a7efhhaefbaBBfaefjx_2nFqxaefhuhuiahefiuha7e",
  	"refresh_token_expiration" : 15589334345 // 60 days
}

//user exists
{
    "code": "user_exists",
    "message": "Email exists, please login",
    "data": {
        "user_id": 5,
      	"OS" : "android" //device used to register
    }
}

//formatting errors
{
    "code": "invalid_email",
    "message": "Invalid email address",
    "data": {
        "status": 404
    }
}

//duplicate purchase token
{
    "code": "token_duplicate",
    "message": "Duplicate purchase token",
    "data": {
        "purchase_token": "aefjiaefoijaeoijf2347h", //duplicate token
        "email": "useremail@test.com" //associated email, if available
    }
}

//expired
{
    "code": "token_expired",
    "message": "Subscription expired", //response from lambda
    "data": {
      //data blank since we don't have a user yet
    }
}

//invalid purchase token
{
    "code": "token_error",
    "message": "Invalid Value", //response from lambda
    "data": {
        //data blank since we don't have a user yet
    }
}
```



### Update user subscription

| Method | Endpoint                              |
| ------ | ------------------------------------- |
| POST   | /wp-json/yc-users/v1/register/update/ |

**Required Parameters**

| Parameter     | Type     | Description                                             |
| ------------- | -------- | ------------------------------------------------------- |
| email         | <string> | user ID for which data is being requested               |
| productId     | <string> | yoga_monthly, yoga_quarterly, yoga_yearly               |
| OS            | <string> | must be 'ios' or 'android'                              |
| purchaseToken | <string> | token provided by app store for subscription validation |

**Example Response**

`/wp-json/yc-users/v1/register/update/`

```json
//success response same as /wp-json/yc-users/v1/register/


//duplicate purchase token
{
    "code": "token_duplicate",
    "message": "Duplicate purchase token",
    "data": {
        "purchase_token": "aefjiaefoijaeoijf2347h", //duplicate token
        "email": "useremail@test.com" //associated email, if available
    }
}

//invalid purchase token
{
    "code": "token_error",
    "message": "Invalid Value", //lambda response
    "data": {
       "status": 403,
       "OS": "android" //platform user used to register - android, ios, or website
    }
}

//expired purchase token, same OS
{
    "code": "token_expired",
    "message": "Subscription expired. Please renew.",
    "data": {
        "status": 403,
        "OS": "android" //platform user used to register - android, ios, or website
    }
}

//expired purchase token, different OS
{
    "code": "token_expired",
    "message": "This email has an existing Android subscription. Please login from your Android device or use another email.",
    "data": {
        "status": 403,
        "OS": "android" //platform user used to register - android, ios, or website
    }
}
```


### Reset Password

| Method | Endpoint                       |
| ------ | ------------------------------ |
| POST   | /wp-json/yc-users/v1/lostpassword/?user_login=<user_login> |

**Required Parameters**

| Parameter | Type      | Description                               |
| --------- | --------- | ----------------------------------------- |
| user_login     | <string>  | User email or username |

**Example Response**

```json
//success
{
	"code": 200,
	"message": "Check your email for password reset instructions"
}

//error
{
	"code": 400,
	"message": "Email address not found"
}
```

### Delete user

| Method | Endpoint                              |
| ------ | ------------------------------------- |
| POST   | /wp-json/yc-users/v1/delete/ |

**Required Parameters**
Token sent as Authorization header is required.


| Parameter     | Type     | Description                                             |
| ------------- | -------- | ------------------------------------------------------- |
| OS            | <string> | 'ios', 'android', 'website'                             |

**Example Response**

```json
//success
{
	"code": 200,
	"message": "Account successfully deleted"
}

//example error
{
	"code": "invalid_user",
	"message": "Invalid user"
}
```


## Classes

### Get all classes

| Method | Endpoint                |
| ------ | ----------------------- |
| GET    | /wp-json/wp/v2/classes/ |

See single class endpoint below for documentation.



### Get single class

| Method | Endpoint                         |
| ------ | -------------------------------- |
| GET    | /wp-json/wp/v2/classes/<classid> |

**Selected JSON Response**

| Attribute                        | Description                                                  |
| -------------------------------- | ------------------------------------------------------------ |
| classes_meta:yc_video_id         | <str> ID of vimeo video for class                            |
| classes_meta:youtube_video_id    | <str> ID of youtube video for clsasclass                     |
| classes_meta:unprotected_content | <str> Workaround - see next field                            |
| content.rendered                 | <str> If unauthenticated, MemberPress returns a default message here instead of the post content |
| classes_meta:teacher             | <array> contains data for teacher including `teacher_meta`   |
| classes_meta:taxonomy            | <array> taxonomy data for each term assigned to class        |
| classes_meta:image               | <str> featured image URL                                     |
| classes_video                    | <array> expiring video URLs from vimeo API                   |
| custom_title                     | <string> decoded title without HTML entities                 |

**Example Response**

`/wp-json/wp/v2/classes/15935`

```json
{
   "id":9278,
   "date":"2018-05-14T22:04:01",
   "date_gmt":"2018-05-14T22:04:01",
   "guid":{
      "rendered":"http:\/Users\/dryan1144\/Desktop\/Dropbox\/David\/Yoga%20Collective\/yogacollective.dev.cc.cc\/classes\/antioxidant-smoothie\/"
   },
   "modified":"2018-06-15T19:23:39",
   "modified_gmt":"2018-06-15T19:23:39",
   "slug":"antioxidant-smoothie",
   "status":"publish",
   "type":"classes",
   "link":"\/classes\/antioxidant-smoothie\/",
   "title":{
      "rendered":"Antioxidant Smoothie"
   },
   "content":{
      "rendered":"<div class=\"mp_wrapper\">\n  <div class=\"mepr-unauthorized-excerpt\">\n    <p>1 Cup Berries 1\/2 Banana Frozen 1\/4 inch slice of Lemon (with peel) Cup of Water 1 Cup Kale or Spinach 1\/2 Avocado 1 tbsp of Apple Cider Vinegar<\/p>\n  <\/div>\n  <div class=\"mepr-unauthorized-message\">\n    <p>You are unauthorized to view this page.<\/p>\n  <\/div>\n  <div class=\"mepr-login-form-wrap\">\n    <\/div>\n<\/div>\n\n",
      "protected":false
   },
   "excerpt":{
      "rendered":"<p>1 Cup Berries 1\/2 Banana Frozen 1\/4 inch slice of Lemon (with peel) Cup of Water 1 Cup Kale or Spinach 1\/2 Avocado 1 tbsp of Apple Cider Vinegar You are unauthorized to view this page.<\/p>\n",
      "protected":false
   },
   "featured_media":7291,
   "template":"",
   "teachers":[
      206
   ],
   "class_types":[
      30
   ],
   "styles":[

   ],
   "focus":[

   ],
   "levels":[

   ],
   "duration":[
      215
   ],
   "featured":[
      240
   ],
   "classes_meta":{
      "yc_video_id":"",
      "youtube_video_id":"GFIx2Oxr7Zw",
      "unprotected_content":"<ul>\r\n \t<li>1 Cup Berries<\/li>\r\n \t<li>1\/2 Banana Frozen<\/li>\r\n \t<li>1\/4 inch slice of Lemon (with peel)<\/li>\r\n \t<li>Cup of Water<\/li>\r\n \t<li>1 Cup Kale or Spinach<\/li>\r\n \t<li>1\/2 Avocado<\/li>\r\n \t<li>1 tbsp of Apple Cider Vinegar<\/li>\r\n<\/ul>",
      "image":"\/wp-content\/uploads\/2015\/06\/YC_Shayna-4-9-15-Antioxidant-Smoothie-Part-6.jpg",
      "taxonomies":{
         "class_types":[
            {
               "name":"Refresh",
               "id":30
            }
         ],
         "duration":[
            {
               "name":"10",
               "id":215
            }
         ],
         "featured":[
            {
               "name":"Trending",
               "id":240
            }
         ]
      },
      "teacher":{
         "name":"Shayna Hiller",
         "teacher_meta":{
            "image":"\/wp-content\/uploads\/2017\/12\/user.png",
            "bio":"",
            "speciality":""
         }
      }
   },
    "classes_video":{  
      "sd":"https:\/\/11-lvl3-pdl.vimeocdn.com\/01\/2834\/4\/114173370\/315796745.mp4?expires=1542336563&token=09cf73c5a7d2dada18857",
      "hd":"https:\/\/11-lvl3-pdl.vimeocdn.com\/01\/2834\/4\/114173370\/315796740.mp4?expires=1542336564&token=0318040a7675884ed8cad"
   },
   "_links":{
      "self":[
         {
            "href":"\/wp-json\/wp\/v2\/classes\/9278"
         }
      ],
      "collection":[
         {
            "href":"\/wp-json\/wp\/v2\/classes"
         }
      ],
      "about":[
         {
            "href":"\/wp-json\/wp\/v2\/types\/classes"
         }
      ],
      "wp:featuredmedia":[
         {
            "embeddable":true,
            "href":"\/wp-json\/wp\/v2\/media\/7291"
         }
      ],
      "wp:attachment":[
         {
            "href":"\/wp-json\/wp\/v2\/media?parent=9278"
         }
      ],
      "wp:term":[
         {
            "taxonomy":"teachers",
            "embeddable":true,
            "href":"\/wp-json\/wp\/v2\/teachers?post=9278"
         },
         {
            "taxonomy":"class_types",
            "embeddable":true,
            "href":"\/wp-json\/wp\/v2\/class_types?post=9278"
         },
         {
            "taxonomy":"styles",
            "embeddable":true,
            "href":"\/wp-json\/wp\/v2\/styles?post=9278"
         },
         {
            "taxonomy":"focus",
            "embeddable":true,
            "href":"\/wp-json\/wp\/v2\/focus?post=9278"
         },
         {
            "taxonomy":"levels",
            "embeddable":true,
            "href":"\/wp-json\/wp\/v2\/levels?post=9278"
         },
         {
            "taxonomy":"duration",
            "embeddable":true,
            "href":"\/wp-json\/wp\/v2\/duration?post=9278"
         },
         {
            "taxonomy":"featured",
            "embeddable":true,
            "href":"\/wp-json\/wp\/v2\/featured?post=9278"
         }
      ],
   }
}
```



## Filters and Search

### Filtering Class Results

**Filter by class-related taxonomy**

`/wp-json/wp/v2/styles`

**Filter by taxonomy term ID**

`/wp-json/wp/v2/classes/?styles=219`

**Filter by taxonomy term name**

`/wp-json/wp/v2/classes?filter[taxonomy]=featured&filter[term]=new`

**Filter by search term**

`/wp-json/wp/v2/classes/?search=smoothie`



### Endpoints for populating Search UI

**All taxonomies and terms**

`/wp/v2/taxonomies`

**Taxonomies and terms for Refresh class_type**

`/yc-filters/v1/refresh-taxonomies`

**Taxonomies and terms for Move class_type**

`/yc-filters/v1/move-taxonomies`

**Taxonomies and terms for Connect class_type**

`/yc-filters/v1/connect-taxonomies`



### Homepage Content

**Search by Focus output**

`/wp-json/yc-focus/v1/featured`

**Featured collections**

`/wp-json/wp/v2/collections?filter[taxonomy]=featured&filter[term]=new`

**Featured/New classes**

`/wp-json/wp/v2/classes?filter[taxonomy]=featured&filter[term]=new`

**Search by Class Type terms** (Limit to first 4 results)

`/wp-json/wp/v2/class_types`



## Favorites

### Get user's favorites

| Method | Endpoint                                |
| ------ | --------------------------------------- |
| GET    | /wp-json/yc-favorites/v1/users/<userid> |

Favorites are class IDs stored in the user meta table for each user. You can request all user data including the user's favorites from `/wp-json/wp/v2/users/<userid>`, or use this custom endpoint to return only the user's favorites.



### Add or remove user's favorites

| Method | Endpoint                                                |
| ------ | ------------------------------------------------------- |
| POST   | /wp-json/yc-favorites/v1/user/<userid>/?class=<classid> |

The same custom endpoint is used to both add and remove a Class from a user's meta. If the class ID provided is in the user's existing favorites array, it will be removed. Otherwise, it will be added.

**Required Parameters**

| Parameter | Type      | Description                               |
| --------- | --------- | ----------------------------------------- |
| userid    | <integer> | user ID for which data is being requested |
| classid   | <integer> | class ID being added/removed              |

**JSON Response** 

| Attribute    | Description                               | Possible Values        |
| ------------ | ----------------------------------------- | ---------------------- |
| code         | <string> what action was taken by the API | favorited, unfavorited |
| message      | <string> explanation of what took place   |                        |
| button_text  | <string> text to use to update button UI  | Favorite, Favorited    |
| tooltip_text | <integer> used by website to update UI    |                        |

**Example Response**

`/wp-json/yc-favorites/v1/user/232/?class=234`

```json
{
	"code": "unfavorited",
	"message": "Class removed from favorites",
	"button_text": "Favorite",
	"tooltip_text": "Add to Favorites",
}
```



## Collections

### Get all collections

| Method | Endpoint                    |
| ------ | --------------------------- |
| GET    | /wp-json/wp/v2/collections/ |

Collections are a custom post type and use a combination of the default post endpoint for return data about the Collection itself, and custom endpoints to update users' collection data. Collections IDs - and the Classes a user completes within that Collection - are stored in a user's meta when a user Starts a Collection.

**JSON Response**

See  JSON response below for **Get single collection** and the [REST API Reference](https://developer.wordpress.org/rest-api/reference/posts/) for more details



### Get single collection

| Method | Endpoint                                  |
| ------ | ----------------------------------------- |
| GET    | /wp-json/wp/v2/collections/<collectionid> |

**Selected JSON Response**

| Attribute                   | Description      |
| --------------------------- | ------------------------------------- |
| collection_meta:description | <str> collection description text     |
| collection_meta:time        | <str> total time of collection        |
| collection_meta:count       | <url> number of classes in collection |
| collection_meta:classes     | <array> classes in collection         |
| teachers                    | <array> array of teachers for collection  |
|intro_video|<integer> vimeo ID of intro video|
|collection_meta:image|<str> featured image URL|
|collection_meta:classes:classes_meta| <array> same as classes endpoint output|

**Example Response**

`/wp-json/wp/v2/collections/323`

```json
{
   "id":323,
   "date":"2017-12-15T18:40:44",
   "date_gmt":"2017-12-15T18:40:44",
   "modified":"2018-10-18T00:20:10",
   "modified_gmt":"2018-10-18T00:20:10",
   "slug":"test-collection",
   "status":"publish",
   "type":"collections",
   "link":"http:\/\/www.theyogacollective.com\/collections\/test-collection\/",
   "title":{
      "rendered":"Test Collection"
   },
   "featured_media":281,
   "template":"",
   "collection_meta":{
      "description":"This is a description of a collection",
      "time":"50m",
      "count":"3",
      "classes":[
         {
            "class_id":"9298",
            "image":"http:\/\/www.theyogacollective.com\/wp-content\/uploads\/2015\/08\/stevejones2.jpg",
            "title":"Meditation Practice"
         },
         {
            "class_id":"9296",
            "image":"http:\/\/www.theyogacollective.com\/wp-content\/uploads\/2015\/08\/stevejones1.jpg",
            "title":"Breath Work"
         },
         {
            "class_id":"9285",
            "image":"http:\/\/www.theyogacollective.com\/wp-content\/uploads\/2015\/06\/Juju-3-19-15-Warrior-Devi-Flow.jpg",
            "title":"Warrior Devi Flow"
         }
      ],
      "teachers":[
         {
            "name":"Andrea Jensen",
            "teacher_meta":{
               "image":"http:\/\/www.theyogacollective.com\/wp-content\/uploads\/2017\/12\/user.png",
               "bio":"",
               "speciality":""
            }
         },
         {
            "name":"Juju Namjai",
            "teacher_meta":{
               "image":"http:\/\/www.theyogacollective.com\/wp-content\/uploads\/2015\/05\/YC_Juju_Headshot_2-200x200.jpg",
               "bio":"This is someone's bio.",
               "speciality":""
            }
         }
      ],
      "intro_video":"2811911"
   },
   "_links":{
      "self":[
         {
            "href":"http:\/\/www.theyogacollective.com\/wp-json\/wp\/v2\/collections\/323"
         }
      ],
      "collection":[
         {
            "href":"http:\/\/www.theyogacollective.com\/wp-json\/wp\/v2\/collections"
         }
      ],
      "about":[
         {
            "href":"http:\/\/www.theyogacollective.com\/wp-json\/wp\/v2\/types\/collections"
         }
      ],
      "wp:featuredmedia":[
         {
            "embeddable":true,
            "href":"http:\/\/www.theyogacollective.com\/wp-json\/wp\/v2\/media\/281"
         }
      ],
      "wp:attachment":[
         {
            "href":"http:\/\/www.theyogacollective.com\/wp-json\/wp\/v2\/media?parent=323"
         }
      ],
   }
}
```



### Enroll or restart user in a collection

| Method | Endpoint                                                     |
| ------ | ------------------------------------------------------------ |
| POST   | /wp-json/yc-collections/v1/start/<userid>?collection=<collectionid> |

Both enrolling and restarting a user in a collection utilize the same endpoint and will save the collection in the user's meta. If the provided collection ID is already in the user’s collection array, the classes within that collection will be removed and the API will return `"code": "restarted"` and `"status":"in-progress"`.

**Required Parameters**

| Parameter    | Type      | Description                               |
| ------------ | --------- | ----------------------------------------- |
| userid       | <integer> | user ID for which data is being requested |
| collectionid | <integer> | collection ID class is being added to     |

**JSON Response**

| Attribute     | Description                                     | Possible Values                     |
| ------------- | ----------------------------------------------- | ----------------------------------- |
| code          | <str> action that has taken place               | already-started, restarted, started |
| message       | <str> description of action                     |                                     |
| button_text   | <str> optionally update UI with new button text |                                     |
| status        | <str> new collection status                     | in-progress, complete               |
| next_url      | <url> URL of first class in Collection          |                                     |
| total_classes | <int> total classes in collection               |                                     |

**Example Response**
`/wp-json/yc-collections/v1/start/223/?collection=656`

```json
//success - started or restarted
{
	"code":"started", //restarted
  "button_text": "Start First Class",
	"message":"You have started this Collection",
	"next_url":"https://theyogacollective.com/classes/meditation-practice/?yc_collection=323",
	"status":"in-progress"
}

//error - try to restart collection that is incomplete
{
	"code":"already-started",
	"message":"You have already started this Collection",
	"status":"in-progress"
}
```



### Remove user from collection

| Method | Endpoint                                                     |
| ------ | ------------------------------------------------------------ |
| POST   | /wp-json/yc-collections/v1/remove/<userid>?collection=<collectionid> |

**Required Parameters**

| Parameter    | Type      | Description                               |
| ------------ | --------- | ----------------------------------------- |
| userid       | <integer> | user ID for which data is being requested |
| collectionid | <integer> | collection ID class is being added to     |

**Example Response**
`/wp-json/yc-collections/v1/remove/223/?9309`

```json
//success
{
	"code":"removed",
	"message":"This Collection has been removed",
}

//error
{
	"code":"not-started",
	"message":"This Collection has not been started yet",
}
```



### Get user's collection data 

| Method | Endpoint                                 |
| ------ | ---------------------------------------- |
| GET    | /wp-json/yc-collections/v1/user/<userid> |

Collections are stored in the user meta table for each user, with the Collection ID as the array key (see Example Response). You can request all user data including the user's collection data from `/wp-json/wp/v2/users/<userid>`, or use this custom endpoint to return just the user's collection data.

**JSON Response** 

| Attribute              | Description                                | Notes                   |
| ---------------------- | ------------------------------------------ | ----------------------- |
| <collectionid>         | <array> collection of IDs user has started | **ID is the array key** |
| <collectionid>:classes | <array> IDs of classes completed           |                         |
| <collectionid>:status  | <str> collection status                    | in-progress, complete   |

**Example Response**
`/wp-json/yc-collections/v1/user/223`

```json
{
    "code": 200,
    "data" : {
        "9333":{
            "classes":[
                9296,
                9298,
                9295
            ],
            "status":"complete"
        },
        "9309":{
            "classes":[9285],
            "status":"in-progress"
        }
    }
    
}
```



### Add or remove class from a user's collection

| Method | Endpoint                                                     |
| ------ | ------------------------------------------------------------ |
| POST   | /wp-json/yc-collections/v1/user/<userid>/?class=<classid>&collection=<collectionid> |

The same endpoint used to get user’s Collection data is also used to both add and remove a Class from a Collection (using POST instead of GET). If the class ID provided is in the user’s completed Classes array for the given Collection ID, it will be removed. Otherwise, it will be added to the Collection’s Classes array.

**Required Parameters**

| Parameter    | Type      | Description                                |
| ------------ | --------- | ------------------------------------------ |
| userid       | <integer> | user ID for which data is being requested  |
| classid      | <integer> | class ID being added to the collection     |
| collectionid | <integer> | collection ID that class is being added to |

**JSON Response** 

| Attribute     | Description                                                | Possible Values      |
| ------------- | ---------------------------------------------------------- | -------------------- |
| code          | <string> what action was taken by the API                  | complete, incomplete |
| message       | <string> explanation of what took place                    |                      |
| button_text   | <string> text to use to update button UI                   |                      |
| class_count | <integer> updated number of classes complete in collection |                      |
| status| <string> updated status of collection |in-progress, complete|

**Example Response**

`/wp-json/yc-collections/v1/user/232/?class=234&collection=999`

```json
{
	"code": "incomplete",
	"message": "Class marked as incomplete",
	"button_text": "Mark Complete",
	"class_count": 1,
    "status": "in-progress",
}
```



## Teachers

### Get all teachers

| Method | Endpoint                 |
| ------ | ------------------------ |
| GET    | /wp-json/wp/v2/teachers/ |

Teachers are a custom taxonomy and use the default taxonomy endpoint. 

**JSON Response**

See Get single teacher for more information as well the [REST API Documention](https://developer.wordpress.org/rest-api/reference/taxonomies/) for taxonomies.



### Get single teacher

| Method | Endpoint                            |
| ------ | ----------------------------------- |
| GET    | /wp-json/wp/v2/teachers/<teacherid> |

**Required Parameters**

| Parameter | Type      | Description                                                  |
| --------- | --------- | ------------------------------------------------------------ |
| teacher   | <integer> | ID of the teacher taxonomy term for which data is being requested |

**Selected JSON Response**

| Attribute                     | Description                                   | **Notes**           |
| ----------------------------- | --------------------------------------------- | ------------------- |
| teacher_meta:image            | <url> pre-formatted teacher profile image url | square aspect ratio |
| teacher_meta:background_image | <url> wide background image                   |                     |
| teacher_meta:bio              | <string> teacher biography                    |                     |
| teacher_meta:speciality       | <string> teacher speciality                   |                     |

**Example Response**

`/wp-json/wp/v2/teachers/199`

```json
{
   "id":199,
   "count":13,
   "description":"",
   "link":"https:\/\/www.theyogacollective.com\/classes\/teachers\/anaswara\/",
   "name":"Anaswara",
   "slug":"anaswara",
   "taxonomy":"teachers",
   "meta":[],
   "teacher_meta":{
      "image":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2014\/07\/thumb-teacher-anaswara1.jpg",
      "bio":"Anaswara is from the Pacific Palisades, located on the California Coast between Malibu and Santa Monica. She is a mom of two and lives with her husband right outside Venice.",
       "speciality": "Vinyasa Flow and Hatha"
   },
   "_links":{
      "self":[
         {
            "href":"https:\/\/www.theyogacollective.com\/wp-json\/wp\/v2\/teachers\/199"
         }
      ],
      "collection":[
         {
            "href":"https:\/\/www.theyogacollective.com\/wp-json\/wp\/v2\/teachers"
         }
      ],
      "about":[
         {
            "href":"https:\/\/www.theyogacollective.com\/wp-json\/wp\/v2\/taxonomies\/teachers"
         }
      ],
      "wp:post_type":[
         {
            "href":"https:\/\/www.theyogacollective.com\/wp-json\/wp\/v2\/classes?teachers=199"
         }
      ],
   }
}
```





## Blog

### Get all blog posts

| Method | Endpoint             |
| ------ | -------------------- |
| GET    | /wp-json/wp/v2/posts |

**JSON Response** 

Response available at https://developer.wordpress.org/rest-api/reference/posts/



### Get single blog post

| Method | Endpoint          |
| ------ | ----------------- |
| GET    | /wp/v2/posts/<id> |

**Selected JSON Response**

| Attribute             | Description                                  |
| --------------------- | -------------------------------------------- |
| blog_meta:author_name | <str> Author Name                            |
| blog_meta:author_bio  | <array> Author description                   |
| custom_title          | <string> decoded title without HTML entities |

Additional response available at https://developer.wordpress.org/rest-api/reference/posts/



### Get single media object (image)

| Method | Endpoint               |
| ------ | ---------------------- |
| GET    | /wp/v2/media/<mediaid> |

**Required Parameters**

| Parameter | Type      | Description                                                  |
| --------- | --------- | ------------------------------------------------------------ |
| media_id  | <integer> | attachment object (image) ID for which data is being requested |

**Selected JSON Response**

| Attribute                          | Description                                |
| ---------------------------------- | ------------------------------------------ |
| media_details:sizes                | <array> contains all available image sizes |
| media_details:sizes:small-portrait | <array> used for teacher images            |
| media_details:sizes:hero           | <array> used for page backgrounds          |

**Example Response**

`/wp-json/wp/v2/media/9724`

```json
{
   "id":9724,
   "date":"2018-01-25T18:13:15",
   "date_gmt":"2018-01-25T18:13:15",
   "guid":{
      "rendered":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1.jpeg"
   },
   "modified":"2018-01-27T03:00:39",
   "modified_gmt":"2018-01-27T03:00:39",
   "slug":"image1-1",
   "status":"inherit",
   "type":"attachment",
   "link":"https:\/\/www.theyogacollective.com\/image1-1\/",
   "title":{
      "rendered":"image1 (1)"
   },
   "author":2,
   "comment_status":"open",
   "ping_status":"closed",
   "template":"",
   "meta":[],
   "description":{
      "rendered":"<p class=\"attachment\"><a href='https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-e1517021625196.jpeg'><img width=\"300\" height=\"300\" src=\"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-300x300.jpeg\" class=\"attachment-medium size-medium\" alt=\"\" srcset=\"https:\/\/47h07141n4wr3s4gyj49ii1d-wpengine.netdna-ssl.com\/wp-content\/uploads\/2018\/01\/image1-1-300x300.jpeg 300w, https:\/\/47h07141n4wr3s4gyj49ii1d-wpengine.netdna-ssl.com\/wp-content\/uploads\/2018\/01\/image1-1-150x150.jpeg 150w, https:\/\/47h07141n4wr3s4gyj49ii1d-wpengine.netdna-ssl.com\/wp-content\/uploads\/2018\/01\/image1-1-768x768.jpeg 768w, https:\/\/47h07141n4wr3s4gyj49ii1d-wpengine.netdna-ssl.com\/wp-content\/uploads\/2018\/01\/image1-1-1200x1200.jpeg 1200w, https:\/\/47h07141n4wr3s4gyj49ii1d-wpengine.netdna-ssl.com\/wp-content\/uploads\/2018\/01\/image1-1-1325x1325.jpeg 1325w, https:\/\/47h07141n4wr3s4gyj49ii1d-wpengine.netdna-ssl.com\/wp-content\/uploads\/2018\/01\/image1-1-e1517021625196.jpeg 400w\" sizes=\"(max-width: 300px) 100vw, 300px\" \/><\/a><\/p>\n"
   },
   "caption":{
      "rendered":""
   },
   "alt_text":"",
   "media_type":"image",
   "mime_type":"image\/jpeg",
   "media_details":{
      "width":400,
      "height":400,
      "file":"2018\/01\/image1-1-e1517021625196.jpeg",
      "sizes":{
         "thumbnail":{
            "file":"image1-1-150x150.jpeg",
            "width":150,
            "height":150,
            "mime_type":"image\/jpeg",
            "source_url":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-150x150.jpeg"
         },
         "medium":{
            "file":"image1-1-300x300.jpeg",
            "width":300,
            "height":300,
            "mime_type":"image\/jpeg",
            "source_url":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-300x300.jpeg"
         },
         "medium_large":{
            "file":"image1-1-768x768.jpeg",
            "width":768,
            "height":768,
            "mime_type":"image\/jpeg",
            "source_url":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-768x768.jpeg"
         },
         "large":{
            "file":"image1-1-1200x1200.jpeg",
            "width":1200,
            "height":1200,
            "mime_type":"image\/jpeg",
            "source_url":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-1200x1200.jpeg"
         },
         "hero-image":{
            "file":"image1-1-1325x1325.jpeg",
            "width":1325,
            "height":1325,
            "mime_type":"image\/jpeg",
            "source_url":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-1325x1325.jpeg"
         },
         "small-portrait":{
            "file":"image1-1-400x400.jpeg",
            "width":400,
            "height":400,
            "mime_type":"image\/jpeg",
            "source_url":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-400x400.jpeg"
         },
         "full":{
            "file":"image1-1-e1517021625196.jpeg",
            "width":400,
            "height":400,
            "mime_type":"image\/jpeg",
            "source_url":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-e1517021625196.jpeg"
         }
      },
   },
   "post":null,
   "source_url":"https:\/\/www.theyogacollective.com\/wp-content\/uploads\/2018\/01\/image1-1-e1517021625196.jpeg",
}
```





## Utility Taxonomies

The Featured taxonomy will control Class and Collection for the app homepage and for the Basic membership. Get tag ID from the Featured taxonomy endpoint and use in your content request. The Featured taxonomy output is also present in the Classes endpoint under `classes_meta`

| Taxonomy | Term  | Description |
| -------- | ----- |--|
| Featured |Basic  |Used for Basic membership Class access|
| Featured |Trending  | Used for app homepage, new section (Classes and Collections) |
| Featured |New  | Used for New label only |

`wp-json/wp/v2/featured`

`wp-json/wp/v2/classes?featured=253`



## Quick Hits

Quick hits uses the `focus` taxonomy.

| Attribute        | Description                                    |
| ---------------- | ---------------------------------------------- |
| focus_meta.image | <array> url of background image                |
| description      | <string> optional description of taxonomy term |

`wp-json/wp/v2/focus`



```json
[
    {
        "id": 293,
        "count": 2,
        "description": "",
        "link": "https://stgyogacollect.wpengine.com/classes/focus/balance/",
        "name": "Balance",
        "slug": "balance",
        "taxonomy": "focus",
        "parent": 0,
        "meta": [],
        "focus_meta": {
            "image": [
                "https://stgyogacollect.wpengine.com/wp-content/uploads/2018/09/Screen-Shot-2018-09-02-at-4.24.52-PM.png",
                657,
                470,
                false
            ]
        },
    },
    {
        "id": 208,
        "count": 10,
        "description": "",
        "link": "https://stgyogacollect.wpengine.com/classes/focus/arm-balances/",
        "name": "Arm Balances",
        "slug": "arm-balances",
        "taxonomy": "focus",
        "parent": 0,
        "meta": [],
        "focus_meta": {
            "image": [
                "https://stgyogacollect.wpengine.com/wp-content/uploads/2018/10/Teal-Bottoms-White-Wall-Compass-Pose.jpeg",
                1000,
                667,
                false
            ]
        },
    }
 ]
```



## Reminders

### Update a User's Reminders

| Method | Endpoint                               |
| ------ | -------------------------------------- |
| POST   | /wp-json/yc-reminders/v1/user/<userid> |

**Required Parameters**

| Parameter   | Type      | Description                                               |
| ----------- | --------- | --------------------------------------------------------- |
| userid      | <integer> | id of user to update                                      |
| status      | <string>  | active or inactive (must be one of these two statuses)    |
| days        | <array>   | array of days in sun, mon, tue format                     |
| hour        | <integer> | hour in format 00 to 24                                   |
| minute      | <integer> | minute in format 00 to 59                                 |
| title       | <string>  | (optional) title of push notification                     |
| description | <string>  | (optional) description underenath push notification title |
| frequency   | <integer> | 0,1, or 2                                                 |

**Example Response**

```json
{
	"code": 200,
	"message": "Reminders updated",
}
```



### Get a User's Reminders

| Method | Endpoint                               |
| ------ | -------------------------------------- |
| GET    | /wp-json/yc-reminders/v1/user/<userid> |

**Example Response**

```json
{
  "status" : "active",
  "days": [
    "sun",
    "fri"
  ],
  "frequency": 2,
  "hour": 14,
  "minute": 35,
  "title" : "This is a title",
  "descriptin" : "This is a description here"
}

//no reminders
{
    "code": 200,
    "status": null,
    "days": null,
    "frequency": null,
    "hour": null,
    "minute": null,
    "title": null,
    "description": "No reminders found"
}
```